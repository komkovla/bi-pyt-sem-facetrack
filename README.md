# Blender Face Track
Blender add-on to generate model of the face from web cam or photo and animate it using video or web cam.

Used technologies:
- Python3
- Blender 2.8+
- MediaPipe - face recognition and tracking

### Install
downdload Face Track.zip and install in Blender properties-addon
blender  v3.0.0 is required
after installation right panel will appear
<a href="https://ibb.co/09ysDsN"><img src="https://i.ibb.co/8xgYKY3/Screenshot-2022-01-02-at-23-18-47.png" alt="Screenshot-2022-01-02-at-23-18-47" border="0"></a>
For better understanding how it works, you can watch a video, with work presentation on:
https://drive.google.com/file/d/1R5YDcuowsHATyv7zprtC8lPwaKdbhBj8/view?usp=sharing