import bpy, bmesh
from . import conf


def hide_rig():
    bpy.data.collections[conf.TRACKER_COL_NAME].hide_viewport = True
    bpy.data.objects['Armature'].hide_set(True)



def add_empty(obj):
    bpy.ops.object.select_all(action='DESELECT')
    obj.select_set(True)
    bpy.ops.object.transform_apply()

    def get_col():
        for col in bpy.data.collections:
            if col.name == conf.TRACKER_COL_NAME:
                return col
        print("new_col")
        face_track_collection = bpy.data.collections.new(conf.TRACKER_COL_NAME)
        bpy.data.collections[conf.FACE_COL_NAME].children.link(face_track_collection)
        return face_track_collection

    if obj.mode == 'EDIT':
        bm = bmesh.from_edit_mesh(obj.data)
        verts = [vert.co for vert in bm.verts]

    else:
        verts = [vert.co for vert in obj.data.vertices]

    # coordinates as tuples
    plain_verts = [vert.to_tuple() for vert in verts]
    for index, coord in enumerate(plain_verts):
        col = get_col()

        obj_empty = bpy.data.objects.new("tracker." + str(index), None)
        col.objects.link(obj_empty)
        obj_empty.location = coord
        obj_empty.scale = (0.15, 0.15, 0.15)



def add_rig(obj):
    # face mesh setup
    trackers = bpy.data.collections['Trackers'].objects
    bones = []
    for tracker in trackers:
        bpy.ops.object.armature_add(enter_editmode=False, location=tracker.location)
        bone = bpy.context.object
        bones.append(bone)
    for bone in bones:
        bone.select_set(True)
    # join armatures in correct order
    bpy.context.view_layer.objects.active = bpy.data.objects['Armature']
    bpy.ops.object.join()
    arm = bpy.data.objects['Armature']
    bpy.ops.object.transform_apply()

    # parent face to bones
    obj.select_set(True)
    arm.select_set(True)
    bpy.ops.object.parent_set(type='ARMATURE_AUTO')

    bpy.ops.object.posemode_toggle()
    for i, bone in enumerate(arm.data.bones):
        arm.data.bones.active = bone
        bpy.ops.pose.constraint_add(type='COPY_LOCATION')
        bpy.context.object.pose.bones[bone.name].constraints["Copy Location"].target = trackers[i]
        bone.select = False
    bpy.ops.object.posemode_toggle()