import sys
import importlib
from .src import conf
sys.path.append("./src/")
bl_info = {
    'name': 'Face Track',
    'category': 'All',
    'version': (0, 0, 1),
    'blender': (3, 0, 0)
}


modulesNames = ['install_dep' ,'add_face', 'prepare_face', 'video_track', 'face_track_panel']
#modulesNames = ['install_dep' , 'face_track_panel']
modulesFullNames = {}
for currentModuleName in modulesNames:
    modulesFullNames[currentModuleName] = ('{}.{}'.format(__name__, currentModuleName))



for currentModuleFullName in modulesFullNames.values():
    if currentModuleFullName in sys.modules:
        print("reload: ", currentModuleFullName)
        importlib.reload(sys.modules[currentModuleFullName])
    else:
        print("import: ", currentModuleFullName)
        globals()[currentModuleFullName] = importlib.import_module(currentModuleFullName)
        setattr(globals()[currentModuleFullName], 'modulesNames', modulesFullNames)


def register():
    conf.init()
    for currentModuleName in modulesFullNames.values():
        if currentModuleName in sys.modules:
            if hasattr(sys.modules[currentModuleName], 'register'):
                sys.modules[currentModuleName].register()
                print('register: ', currentModuleName)

def unregister():
    for currentModuleName in modulesFullNames.values():
        if currentModuleName in sys.modules:
            if hasattr(sys.modules[currentModuleName], 'unregister'):
                sys.modules[currentModuleName].unregister()
if __name__ == "__main__":
    register()