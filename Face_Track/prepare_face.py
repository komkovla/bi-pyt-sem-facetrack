import bpy, bmesh
from .src import conf
from .src.prepare_face_p import *




class Prepare_face(bpy.types.Operator):
    bl_idname = 'mesh.prepare_face'
    bl_label = 'Prepare Face'
    bl_options = {"REGISTER", "UNDO"}
    def execute(self, context):
        obj = bpy.context.active_object
        if obj == None:
            conf.ShowMessageBox("Choose the face you want to prepare")
            return {"FINISHED"}
        if conf.FACE_NAME == obj.name:
            add_empty(obj)
            add_rig(obj)
            hide_rig()
            conf.FACE_PREPARED = True
        else:
            conf.ShowMessageBox("Only faces created with addon are supported")
        return {"FINISHED"}    
        

    @classmethod
    def poll(cls, context):
        return conf.MODULES_INSTALLED and not conf.FACE_PREPARED


def register():
    bpy.utils.register_class(Prepare_face)


def unregister():
    bpy.utils.unregister_class(Prepare_face)




