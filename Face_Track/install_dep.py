import bpy
from .src import conf
import sys
import pathlib
import subprocess


class installDependencies(bpy.types.Operator):
    bl_idname = 'preferences.install_dep'
    bl_label = 'install dependencies'
    bl_options = {"REGISTER", "UNDO"}
    def execute(self, context):
        if install(self) != 0:
            return {"FINISHED"}
        conf.MODULES_INSTALLED = True
        print('install: ', conf.MODULES_INSTALLED)
        return {"FINISHED"}

    @classmethod
    def poll(cls, context):
        return not conf.MODULES_INSTALLED
def register():
    bpy.utils.register_class(installDependencies)




def unregister():
    bpy.utils.unregister_class(installDependencies)


def install(context):


    install_path = pathlib.Path()
    for i in sys.path:
        if "site-packages" in i:
            install_path = pathlib.Path(i)
            global DEP_PATH
            DEP_PATH = install_path
            break
    if 'site-packages' not in str(install_path):
        print("Path not found")
        conf.ShowMessageBox("Something Went Wrong")
        return -1
    dir = str(install_path)
    subprocess.run([sys.executable, '-m', 'pip', 'install', "--upgrade", 'pip', ], check=True)
    for i in range(0, len(conf.package_names)):
        context.report({'INFO'}, "Installing %s" % conf.package_names[i].name)
        conf.package_names[i] = conf.Package(conf.package_names[i].name, conf.package_names[i].installed, True)
        subprocess.run([sys.executable, '-m', 'pip', "install", '--target', dir, conf.package_names[i].name, ], check=True)
        conf.package_names[i] = conf.Package(conf.package_names[i].name, True, False)
    return 0