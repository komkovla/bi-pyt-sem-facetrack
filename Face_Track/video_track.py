from bpy.props import StringProperty
from .src.video_track_p import *
from bpy_extras.io_utils import ImportHelper





class Video_track_camera(bpy.types.Operator):
    bl_idname = 'mesh.video_track_camera'
    bl_label = 'from Camera'
    bl_options = {"REGISTER", "UNDO"}

    def execute(self, context):
        trackers = get_trackers()
        if trackers == None:
            return {"FINISHED"}
        video_tracking()
        return {"FINISHED"}

    @classmethod
    def poll(cls, context):
        return conf.MODULES_INSTALLED and conf.FACE_PREPARED

class Video_track_video(bpy.types.Operator, ImportHelper):
    bl_idname = 'mesh.video_track_video'
    bl_label = 'from Video'
    bl_options = {"REGISTER", "UNDO"}

    filter_glob: StringProperty(
        default='*.MOV;*.MP4;*.avi',
        options={'HIDDEN'}
    )
    def execute(self, context):
        trackers = get_trackers()
        if trackers == None:
            return {"FINISHED"}
        video_tracking(webcam_input= False, path = self.filepath)
        return {"FINISHED"}

    @classmethod
    def poll(cls, context):
        return conf.MODULES_INSTALLED and conf.FACE_PREPARED


def register():
    bpy.utils.register_class(Video_track_camera)
    bpy.utils.register_class(Video_track_video)


def unregister():
    bpy.utils.unregister_class(Video_track_camera)
    bpy.utils.unregister_class(Video_track_video)

