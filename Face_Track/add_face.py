from bpy.props import StringProperty
from .src.add_face_p import *

from .src import conf
from bpy_extras.io_utils import ImportHelper





class addFace_camera(bpy.types.Operator):
    bl_idname = 'mesh.add_face_camera'
    bl_label = 'From Camera'
    bl_options = {"REGISTER", "UNDO"}

    def execute(self, context):
        cv2 = conf.import_module('cv2')
        mp = conf.import_module('mediapipe')
        if cv2 == None or mp == None:
            return
        img = capture_img(cv2)
        face_generator(img, cv2, mp)
        return {"FINISHED"}

    @classmethod
    def poll(cls, context):
        return conf.MODULES_INSTALLED

class addFace_photo(bpy.types.Operator, ImportHelper):
    bl_idname = 'mesh.add_face_photo'
    bl_label = 'From photo'
    bl_options = {"REGISTER", "UNDO"}
    filter_glob: StringProperty(
        default='*.jpg;*.jpeg;*.png;*.tif;*.tiff;*.bmp',
        options={'HIDDEN'}
    )
    def execute(self, context):
        cv2 = conf.import_module('cv2')
        mp = conf.import_module('mediapipe')
        if cv2 == None or mp == None:
            return
        img = cv2.imread(self.filepath)
        face_generator(img, cv2, mp)
        return {"FINISHED"}

    @classmethod
    def poll(cls, context):
        return conf.MODULES_INSTALLED


def register():
    bpy.utils.register_class(addFace_camera)
    bpy.utils.register_class(addFace_photo)


def unregister():
    bpy.utils.unregister_class(addFace_camera)
    bpy.utils.unregister_class(addFace_photo)


