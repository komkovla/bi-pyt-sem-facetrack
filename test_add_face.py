import pytest
import cv2
from Face_Track.src.add_face_p import proccess_image
import mediapipe as mp
def test_1():
    for i in range(0, 5):
        image = cv2.imread('tests/foto_face/{0}.jpg'.format(i))
        res,_ = proccess_image(image, cv2, mp)
        assert res == 0
    for i in range(0, 5):
        image = cv2.imread('tests/foto_no_face/{0}.jpg'.format(i))
        res,_ = proccess_image(image, cv2, mp)
        assert res == -2